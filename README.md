
[量化交易的十年研究历程，以及版本升级。](https://mp.weixin.qq.com/s/IJ8C4EBY19sbrXwbVx7Tlg)



始于2013年的最初理念：

理念：基于高低点差的统计学评估。

![输入图片说明](https://mmbiz.qpic.cn/mmbiz_jpg/jI45hvaQStickFaibcdTibWXmricDGvs0trIygHcH51sR8ThYpeLwsE82coQic4p6sFsRYU8WROvg0D5HA2vSgWkQNA/640?wx_fmt=jpeg&tp=webp&wxfrom=5&wx_lazy=1&wx_co=1 "在这里输入图片标题")
![输入图片说明](https://mmbiz.qpic.cn/mmbiz_jpg/jI45hvaQStickFaibcdTibWXmricDGvs0trIdnA6U2HaCvV5uUJWr47Rakib1TsV2FBRZFibBXkOgEtZl9IpDryCQBjw/640?wx_fmt=jpeg&tp=webp&wxfrom=5&wx_lazy=1&wx_co=1 "在这里输入图片标题")




MS量化交易 Web 1.0 官网上线（2015-3）：

思路：最基础简陋版本的工具网站。









MS量化交易 APP 1.0（2015-6）：

思路：移动端的数据聚合，数据驱动。











证券从业资格证考试通过（2015-10）：

思路：既然热爱，那就考个证玩玩。







MS量化交易 Web 2.0 官网备案（2015-12）：

思路：综合性的量化交易评估系统。

MS量化交易系统，一个集成了基础数据、统计分析、策略模型、量化方案，主要核心模块的股票综合评估量化系统。









MS量化交易 APP 2.0（2016-1）：

思路：移动端版本升级，探索社交属性，及少量图表。













MS量化交易 QQ群（2016-7）：

思路：粉丝间互相交流，产品使用反馈。







MS量化分析 微信小程序（2018-4）：

思路：终端轻量化，小程序更便捷的使用体验。













马桶量化 Web 3.0（2019-5）：

理念：构建一个可量化、严谨的交易策略评估模型。







马桶量化 公众号（2019-6）：

思路：构建品牌，汇聚粉丝，研究文章归类。







马桶量化 APP 3.0（2020-2）：

理念：移动端更优、更简洁的量化方案模型，图表驱动。













更多探索，还在持续进行中，只因为，还热爱，美式常伴吾身！





如果愿意，可以打赏一杯美式咖啡☕️，继续思考：




